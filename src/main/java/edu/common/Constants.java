package edu.common;

public class Constants {

    private Constants() { }

    public static final String XML_HEADER                           = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    public static final String XML_DEFAULT_ROOT_START               = "<root>";
    public static final String XML_DEFAULT_ROOT_END                 = "</root>";

    public static final String ERROR_CONVERT_FILE                   = "cannot process file";
    public static final String ERROR_CONVERT_JSON                   = "cannot process json";
    public static final String ERROR_CONVERT_XML                    = "cannot process xml";
    public static final String ERROR_CONVERT_CSV                    = "cannot process csv";
}
