package edu.exception;

public class XPathException extends Exception {

    public XPathException(String message) {
        super(message);
    }
}
