package edu.exception;

public class SqliteException extends Exception {

    public SqliteException(String message) {
        super(message);
    }
}
