package edu.file;

public class DocumentConstants {

    private DocumentConstants() { }

    public static final String EMPLOYEE_ID             = "Id";
    public static final String EMPLOYEE_LAST_NAME      = "LastName";
    public static final String EMPLOYEE_FIRST_NAME     = "FirstName";
    public static final String EMPLOYEE_COUNTRY        = "Country";
    public static final String EMPLOYEE_SALARY         = "Salary";
    public static final String EMPLOYEE_SALARY_TOTAL   = "Total Salary";
}
